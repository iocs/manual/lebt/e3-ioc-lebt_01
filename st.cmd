require essioc
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("LEBT_P", "LEBT-CS:")

require sorensen, 1.2.5
#- Sorensen @ SGA30x501d [SOLENOIDS]
epicsEnvSet("MAGIC_NUMBER", "8.291E-4")  #  This calibration was done by the manufacturer and it was never transparent to us.

epicsEnvSet("SOL01_R", "PwrC-PSSol-01:")
epicsEnvSet("SOL01_HOST", "lebt-solps1.tn.esss.lu.se")
epicsEnvSet("SOL01_CHANNEL", "SolPS-01")
epicsEnvSet("SOL01_INTERLOCK", "LEBT-LPS::PwrC-PSSol-01_EnCmd-R")

epicsEnvSet("SOL02_R", "PwrC-PSSol-02:")
epicsEnvSet("SOL02_HOST", "lebt-solps2.tn.esss.lu.se")
epicsEnvSet("SOL02_CHANNEL", "SolPS-02")
epicsEnvSet("SOL02_INTERLOCK", "LEBT-LPS::PwrC-PSSol-02_EnCmd-R")

iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=$(SOL01_CHANNEL), IP_addr=$(SOL01_HOST), P=$(LEBT_P), R=$(SOL01_R), Amp2Tesla_calib=$(MAGIC_NUMBER)")
iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=$(SOL02_CHANNEL), IP_addr=$(SOL02_HOST), P=$(LEBT_P), R=$(SOL02_R), Amp2Tesla_calib=$(MAGIC_NUMBER)")

dbLoadRecords("db/Solenoids_Watchdog.db", "P=$(LEBT_P), R=$(SOL01_R), Interlock_PV=$(SOL01_INTERLOCK)")
dbLoadRecords("db/Solenoids_Watchdog.db", "P=$(LEBT_P), R=$(SOL02_R), Interlock_PV=$(SOL02_INTERLOCK)")
dbLoadRecords("db/Operational_Parameters.db")

iocInit()
